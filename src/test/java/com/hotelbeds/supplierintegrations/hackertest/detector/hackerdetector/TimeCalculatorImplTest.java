package com.hotelbeds.supplierintegrations.hackertest.detector.hackerdetector;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 *
 * @author luan.martins
 */
@SpringBootTest
public class TimeCalculatorImplTest {

    /**
     * The instance
     */
    @Autowired
    private TimeCalculatorImpl instance;

    /**
     * Test of getMinutesBetweenTimes method, of class TimeCalculatorImpl.
     */
    @Test
    public void testGetMinutesBetweenTimes() {
        String time1 = "Thu, 21 Dec 2000 16:01:07 +0200";
        String time2 = "Thu, 21 Dec 2000 16:01:07 +0200";

        assertEquals(0, instance.getMinutesBetweenTimes(time1, time2));
    }

    /**
     * Test of getMinutesBetweenTimesPlusMinute method, of class
     * TimeCalculatorImpl.
     */
    @Test
    public void testGetMinutesBetweenTimesPlusMinute() {
        String time1 = "Thu, 21 Dec 2000 16:01:07 +0200";
        String time2 = "Thu, 21 Dec 2000 16:02:07 +0200";

        assertEquals(1, instance.getMinutesBetweenTimes(time1, time2));
    }

}
