package com.hotelbeds.supplierintegrations.hackertest.detector.hackerdetector;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 *
 * @author luan.jimenez
 */
@SpringBootTest
public class HackerDetectorImplTest {

    @Autowired
    private HackerDetectorImpl instance;

    /**
     * Test of parseLine method, of class HackerDetectorImpl.
     */
    @Test
    public void testValidParseLine() {
        assertNull(instance.parseLine("80.238.9.179,1542110400,SIGNIN_FAILURE,Will.Smith"));
        assertNull(instance.parseLine("80.238.9.179,1542110460,SIGNIN_FAILURE,Will.Smith"));
        assertNull(instance.parseLine("80.238.9.179,1542110520,SIGNIN_FAILURE,Will.Smith"));
        assertNull(instance.parseLine("80.238.9.179,1542110580,SIGNIN_FAILURE,Will.Smith"));
        assertEquals("80.238.9.179", instance.parseLine("80.238.9.179,1542110699,SIGNIN_FAILURE,Will.Smith"));
    }

    /**
     * Test of parseLine method, of class HackerDetectorImpl.
     */
    @Test
    public void testInvalidValidParseLine() {
        assertNull(instance.parseLine("80.238.9.179,1542110400,SIGNIN_FAILURE,Will.Smith"));
        assertNull(instance.parseLine("80.238.9.179,1542110460,SIGNIN_FAILURE,Will.Smith"));
        assertNull(instance.parseLine("80.238.9.179,1542110520,SIGNIN_FAILURE,Will.Smith"));
        assertNull(instance.parseLine("80.238.9.179,1542110580,SIGNIN_FAILURE,Will.Smith"));
        assertNull(instance.parseLine("80.238.9.179,1542110700,SIGNIN_FAILURE,Will.Smith"));
    }

}
