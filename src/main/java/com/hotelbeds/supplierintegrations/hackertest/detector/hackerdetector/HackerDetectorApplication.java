package com.hotelbeds.supplierintegrations.hackertest.detector.hackerdetector;

import java.io.BufferedReader;
import java.io.FileReader;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 *
 * @author luan.jimenez
 */
@SpringBootApplication
public class HackerDetectorApplication {

    public static void main(String[] args) {
        SpringApplication.run(HackerDetectorApplication.class, args);
    }

    @Bean
    public CommandLineRunner run(@Autowired HackerDetectorImpl hackerDetectorImpl) {
        return (args) -> {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("attempts.log"));
            String row;

            while (!StringUtils.isBlank(row = bufferedReader.readLine())) {
                String ipAddress = hackerDetectorImpl.parseLine(row);

                if (StringUtils.isNotBlank(ipAddress)) {
                    System.out.println(ipAddress);
                }
            }
        };
    }

}
