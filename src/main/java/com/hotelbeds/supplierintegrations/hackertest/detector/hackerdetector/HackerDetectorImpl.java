package com.hotelbeds.supplierintegrations.hackertest.detector.hackerdetector;

import com.hotelbeds.supplierintegrations.hackertest.detector.hackerdetector.enumerator.ActionEnum;
import com.hotelbeds.supplierintegrations.hackertest.detector.hackerdetector.model.AttemptsLog;
import com.hotelbeds.supplierintegrations.hackertest.detector.hackerdetector.service.AttemptAnaliserService;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author luan.jimenez
 */
@Component
@Slf4j
public class HackerDetectorImpl implements HackerDetector {

    @Autowired
    AttemptAnaliserService attemptAnaliserService;

    @Override
    public synchronized String parseLine(String line) {
        String[] data = StringUtils.split(line, ",");

        AttemptsLog attemptsLog = AttemptsLog.builder().
            ipAddress(data[0]).
            dateTime(parseEpochToLocalDateTime(data[1])).
            actionEnum(StringUtils.equals(data[2], ActionEnum.SIGNIN_SUCCESS.name()) ? ActionEnum.SIGNIN_SUCCESS : ActionEnum.SIGNIN_FAILURE).
            userName(data[3]).
            build();

        log.info(StringUtils.join("Parse line: ", attemptsLog.toString()));

        if (attemptsLog.getActionEnum().equals(ActionEnum.SIGNIN_FAILURE) && attemptAnaliserService.isInvalidIpAddress(attemptsLog)) {
            return attemptsLog.getIpAddress();
        } else {
            return null;
        }
    }

    private LocalDateTime parseEpochToLocalDateTime(String epoch) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.valueOf(epoch) * 1000), ZoneId.systemDefault());
    }

}
