package com.hotelbeds.supplierintegrations.hackertest.detector.hackerdetector.enumerator;

/**
 *
 * @author luan.jimenez
 */
public enum ActionEnum {

    SIGNIN_SUCCESS,
    SIGNIN_FAILURE

}
