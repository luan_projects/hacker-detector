package com.hotelbeds.supplierintegrations.hackertest.detector.hackerdetector;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

/**
 *
 * @author luan.jimenez
 */
@Component
public class TimeCalculatorImpl implements TimeCalculator {

    @Override
    public Long getMinutesBetweenTimes(String time1, String time2) {
        ZonedDateTime zonedDateTime1 = ZonedDateTime.parse(time1, DateTimeFormatter.RFC_1123_DATE_TIME);
        ZonedDateTime zonedDateTime2 = ZonedDateTime.parse(time2, DateTimeFormatter.RFC_1123_DATE_TIME);

        return Math.abs(Duration.between(zonedDateTime1, zonedDateTime2).toMinutes());
    }

}
