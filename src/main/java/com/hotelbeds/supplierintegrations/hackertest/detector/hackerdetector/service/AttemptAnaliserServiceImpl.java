package com.hotelbeds.supplierintegrations.hackertest.detector.hackerdetector.service;

import com.hotelbeds.supplierintegrations.hackertest.detector.hackerdetector.model.AttemptsLog;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 *
 * @author luan.jimenez
 */
@Service
@Slf4j
public class AttemptAnaliserServiceImpl implements AttemptAnaliserService {

    private static final Map<String, LocalDateTime> failedLoginMap = new ConcurrentHashMap<>();

    private LocalDateTime firstAttempt;

    private int attempts = 1;

    @Override
    public Boolean isInvalidIpAddress(AttemptsLog attemptsLog) {
        for (Entry<String, LocalDateTime> entry : failedLoginMap.entrySet()) {
            if (firstAttempt.plusMinutes(5).isBefore(attemptsLog.getDateTime())) {
                failedLoginMap.remove(entry.getKey());
            }
        }

        if (failedLoginMap.get(attemptsLog.getIpAddress()) == null) {
            firstAttempt = attemptsLog.getDateTime();
            failedLoginMap.put(attemptsLog.getIpAddress(), attemptsLog.getDateTime());
        }

        if (firstAttempt.isBefore(attemptsLog.getDateTime()) && firstAttempt.plusMinutes(5).isAfter(attemptsLog.getDateTime())) {
            attempts++;
        } else {
            attempts = 1;
            firstAttempt = attemptsLog.getDateTime();
        }
        return attempts >= 5;
    }

}
