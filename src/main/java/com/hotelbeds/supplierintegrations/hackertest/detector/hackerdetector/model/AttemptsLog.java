package com.hotelbeds.supplierintegrations.hackertest.detector.hackerdetector.model;

import com.hotelbeds.supplierintegrations.hackertest.detector.hackerdetector.enumerator.ActionEnum;
import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author luan.jimenez
 */
@Data
@Builder
public class AttemptsLog {

    private final String ipAddress;

    private final LocalDateTime dateTime;

    private final ActionEnum actionEnum;

    private final String userName;

}
