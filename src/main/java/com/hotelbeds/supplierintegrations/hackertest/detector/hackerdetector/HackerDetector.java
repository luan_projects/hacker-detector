package com.hotelbeds.supplierintegrations.hackertest.detector.hackerdetector;

/**
 *
 * @author luan.jimenez
 */
public interface HackerDetector {

    String parseLine(String line); 

}
