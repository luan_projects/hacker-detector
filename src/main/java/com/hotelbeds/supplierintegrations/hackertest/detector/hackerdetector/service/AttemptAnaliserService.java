package com.hotelbeds.supplierintegrations.hackertest.detector.hackerdetector.service;

import com.hotelbeds.supplierintegrations.hackertest.detector.hackerdetector.model.AttemptsLog;

/**
 *
 * @author luan.jimenez
 */
public interface AttemptAnaliserService {

    Boolean isInvalidIpAddress(AttemptsLog attemptsLog);

}
