package com.hotelbeds.supplierintegrations.hackertest.detector.hackerdetector;

/**
 *
 * @author luan.jimenez
 */
public interface TimeCalculator {

    Long getMinutesBetweenTimes(String time1, String time2);

}
